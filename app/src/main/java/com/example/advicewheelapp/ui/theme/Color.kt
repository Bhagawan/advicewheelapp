package com.example.advicewheelapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)



val Red = Color(0xFFB61313)
val Orange = Color(0xFFC54205)
val Yellow = Color(0xFFDB9C2D)
val Green = Color(0xFF36812E)
val Blue_light = Color(0xFF48B1DA)
val Blue = Color(0xFF0D44A5)
val Violet = Color(0xFF6E34A2)
val Transparent_grey = Color(0x80000000)
val Yellow_light = Color(0xFFFFE6A8)
val Brown = Color(0xFF642705)