package com.example.advicewheelapp.ui.screens.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.advicewheelapp.R
import com.example.advicewheelapp.data.static.UrlBack
import com.example.advicewheelapp.ui.composables.AdvicePopup
import com.example.advicewheelapp.ui.view.WheelView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun MainScreen() {
    val viewModel = viewModel(MainScreenViewModel::class.java)
    var init = remember { true }

    val advice by viewModel.advicePopup.collectAsState()

    Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        AndroidView(factory = { WheelView(it) },
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1.0f),
            update = { view ->
                if(init) {
                    view.setInterface(object : WheelView.WheelInterface {
                        override fun finish() {
                            viewModel.showAdvice()
                        }
                    })
                    viewModel.restartFlow.onEach {
                        if(it) view.restart()
                    }.launchIn(viewModel.viewModelScope)
                    init = false
                }
            })

        Spacer(modifier = Modifier.weight(1.0f))

        Box(modifier = Modifier
            .clickable(MutableInteractionSource(), null) { viewModel.restart() }
            .padding(bottom = 30.dp)
            .fillMaxWidth(0.8f)
            .height(100.dp), contentAlignment = Alignment.Center) {
            Image(painterResource(id = R.drawable.ic_btn_yeellow), contentDescription = null, modifier = Modifier.fillMaxSize().background(Color.Transparent), contentScale = ContentScale.FillBounds)
            Text(stringResource(id = R.string.btn_start), textAlign = TextAlign.Center, fontSize = 20.sp, color = Color.Black)
        }
    }
    AdvicePopup(advice = advice, viewModel::hideAdvice)
}