package com.example.advicewheelapp.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.advicewheelapp.R
import com.example.advicewheelapp.ui.theme.Brown
import com.example.advicewheelapp.ui.theme.Transparent_grey
import com.example.advicewheelapp.ui.theme.Yellow_light

@Composable
fun AdvicePopup(advice: String?, onClose: () -> Unit) {
    var state by remember { mutableStateOf(!advice.isNullOrEmpty()) }
    var text by remember { mutableStateOf(advice?: "") }
    if(!advice.isNullOrEmpty()) text = advice
    state = !advice.isNullOrEmpty()
    AnimatedVisibility(
        visible = state,
        enter = fadeIn(tween(1000)),
        exit = fadeOut(tween(1))) {
        Box(modifier = Modifier
            .fillMaxSize()
            .clickable(MutableInteractionSource(), null) { onClose() }
            .background(Transparent_grey), contentAlignment = Alignment.Center) {
            Box(modifier = Modifier
                .fillMaxWidth(0.8f)
                .wrapContentHeight()) {
                Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxWidth(0.9f)
                    .background(Yellow_light, RoundedCornerShape(20.dp))
                    .align(Alignment.Center)
                    .border(width = 2.dp, color = Brown, shape = RoundedCornerShape(20.dp))
                    .padding(vertical = 30.dp, horizontal = 10.dp)) {
                    Text(text = text, color = Color.Black, textAlign = TextAlign.Center, fontSize = 15.sp)
                }
                Image(painterResource(id = R.drawable.ic_decorative_leafs), contentDescription = null, modifier = Modifier.fillMaxWidth().height(25.dp).align(
                    Alignment.TopCenter), contentScale = ContentScale.FillBounds)
                Image(painterResource(id = R.drawable.ic_decorative_leafs), contentDescription = null, modifier = Modifier.fillMaxWidth().height(25.dp).align(
                    Alignment.BottomCenter), contentScale = ContentScale.FillBounds)
            }
        }
    }
}