package com.example.advicewheelapp.ui.view

import android.content.Context
import android.graphics.*
import android.view.View
import androidx.compose.ui.graphics.toArgb
import com.example.advicewheelapp.data.static.CurrentAppData
import com.example.advicewheelapp.ui.theme.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.cos
import kotlin.math.sin

class WheelView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var radius = 0
    private var pinH = 1
    private var pinW = 1

    private var angle = 0
    private var speed = 0.0f

    private val colors = listOf(
        Red,
        Orange,
        Yellow,
        Green,
        Blue_light,
        Blue,
        Violet
    )

    private val wheelBorderBitmap = CurrentAppData.wheelBorder?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val wheelPinBitmap = CurrentAppData.wheelPin?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)

    private var mInterface: WheelInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            radius = mWidth.coerceAtMost(mHeight) / 2
            pinW = radius / 7
            pinH =  (pinW / wheelPinBitmap.width.toFloat() * wheelPinBitmap.height.toFloat()).toInt()
        }
    }

    override fun onDraw(canvas: Canvas) {
        updateWheel()
        drawWheel(canvas)
    }

    //// Public

    fun setInterface(i: WheelInterface) {
        mInterface = i
    }

    fun restart() {
        speed = 30.0f
    }

    //// Private

    private fun drawWheel(c: Canvas) {
        val p = Paint()
        p.style = Paint.Style.FILL

        val dA = 360.0f / colors.size
        for(color in colors.withIndex()) {
            val path = Path()
            path.moveTo(mWidth / 2.0f, mHeight / 2.0f)
            var n = angle + color.index * dA
            while(n <= angle + color.index * dA + dA + 1) {
                val x = cos(Math.toRadians(n.toDouble())) * radius * 0.95f + mWidth / 2.0f
                val y = sin(Math.toRadians(n.toDouble())) * radius * 0.95f + mHeight / 2.0f
                path.lineTo(x.toFloat(),y.toFloat())
                n++
            }
            path.close()
            p.color = color.value.toArgb()
            c.drawPath(path, p)
        }

        c.drawBitmap(wheelBorderBitmap, null, Rect(mWidth / 2 - radius, mHeight / 2 - radius,mWidth / 2 + radius,mHeight / 2 + radius,), p)
        c.drawBitmap(wheelPinBitmap, null, Rect((mWidth - pinW) / 2, mHeight / 2 - radius,(mWidth + pinW) / 2,mHeight / 2 - radius + pinH,), p)
    }

    private fun updateWheel() {
        if(speed > 0) {
            speed-=0.5f
            angle = (angle + speed).toInt() % 360
            if(speed <= 0) mInterface?.finish()
        }
    }

    interface WheelInterface {
        fun finish()
    }
}