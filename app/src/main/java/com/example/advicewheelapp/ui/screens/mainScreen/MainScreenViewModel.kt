package com.example.advicewheelapp.ui.screens.mainScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advicewheelapp.data.static.CurrentAppData
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainScreenViewModel: ViewModel() {
    private val _restartFlow = MutableSharedFlow<Boolean>(0)
    val restartFlow = _restartFlow.asSharedFlow()

    private val _advicePopup = MutableStateFlow<String?>(null)
    val advicePopup = _advicePopup.asStateFlow()

    fun restart() {
        viewModelScope.launch {
            _restartFlow.emit(true)
        }
    }

    fun showAdvice() {
        _advicePopup.tryEmit(CurrentAppData.advices.random())
    }

    fun hideAdvice() {
        _advicePopup.tryEmit(null)
    }
}