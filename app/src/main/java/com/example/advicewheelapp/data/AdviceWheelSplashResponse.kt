package com.example.advicewheelapp.data

import androidx.annotation.Keep

@Keep
data class AdviceWheelSplashResponse(val url : String)