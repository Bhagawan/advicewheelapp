package com.example.advicewheelapp.data.static

const val UrlSplash = "AdviceWheelApp/splash.php"
const val UrlLogo = "http://195.201.125.8/AdviceWheelApp/logo.png"
const val UrlBack = "http://195.201.125.8/AdviceWheelApp/back.png"
const val UrlAdvices = "http://195.201.125.8/AdviceWheelApp/advices.json"
const val UrlWheelBorder = "http://195.201.125.8/AdviceWheelApp/border.png"
const val UrlWheelPin = "http://195.201.125.8/AdviceWheelApp/pin.png"