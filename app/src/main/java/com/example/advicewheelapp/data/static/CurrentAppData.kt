package com.example.advicewheelapp.data.static

import android.graphics.Bitmap

object CurrentAppData {
    var url = ""
    var advices = emptyList<String>()

    var wheelBorder: Bitmap? = null
    var wheelPin: Bitmap? = null
}