package com.example.advicewheelapp

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advicewheelapp.data.static.CurrentAppData
import com.example.advicewheelapp.ui.screens.Screens
import com.example.advicewheelapp.util.AssetLoader
import com.example.advicewheelapp.util.api.AdviceWheelServerClient
import com.example.advicewheelapp.util.navigation.Navigator
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class AdviceWheelMainViewModel: ViewModel() {
    private var request: Job? = null
    private val server = AdviceWheelServerClient.create()
    private var assetLoader: AssetLoader? = null

    // Public

    fun init(service: String, id: String, loader: AssetLoader) {
        assetLoader = loader
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun reloadAssets() {
        switchToApp()
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToApp()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToApp()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToApp()
                } else switchToApp()
            }
        } catch (e: Exception) {
            switchToApp()
        }
    }

    private fun switchToApp() {
        assetLoader?.setInterface(object : AssetLoader.AssetLoaderInterface {
            override fun onAssetsLoaded() {
                Navigator.navigateTo(Screens.MAIN_SCREEN)
            }

            override fun onError() {
                Navigator.navigateTo(Screens.NETWORK_ERROR_SCREEN)
            }
        })
        assetLoader?.loadAssets()
    }
}