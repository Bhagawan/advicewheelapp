package com.example.advicewheelapp.util.api

import com.example.advicewheelapp.data.AdviceWheelSplashResponse
import com.example.advicewheelapp.data.static.UrlAdvices
import com.example.advicewheelapp.data.static.UrlSplash
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface AdviceWheelServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<AdviceWheelSplashResponse>


    @GET(UrlAdvices)
    suspend fun getAdvices(): Response<List<String>>

    companion object {
        fun create() : AdviceWheelServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(AdviceWheelServerClient::class.java)
        }
    }
}