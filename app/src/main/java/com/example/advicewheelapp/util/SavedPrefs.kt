package com.example.advicewheelapp.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("Quantum", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        private inline fun <reified T> Gson.fromJson(json: String): T = fromJson(json, object: TypeToken<T>() {}.type)
    }
}