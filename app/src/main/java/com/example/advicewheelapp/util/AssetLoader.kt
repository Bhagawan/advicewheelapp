package com.example.advicewheelapp.util

import android.content.Context
import com.example.advicewheelapp.data.static.CurrentAppData
import com.example.advicewheelapp.data.static.UrlWheelBorder
import com.example.advicewheelapp.data.static.UrlWheelPin
import com.example.advicewheelapp.util.api.AdviceWheelServerClient
import com.example.advicewheelapp.util.api.ImageDownloader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

class AssetLoader(private val context: Context) {
    private var mInterface: AssetLoaderInterface? = null
    private val scope = CoroutineScope(EmptyCoroutineContext)

    fun setInterface(i: AssetLoaderInterface) {
        mInterface = i
    }

    fun loadAssets() {
        scope.launch {
            val advicesRequest = AdviceWheelServerClient.create().getAdvices()
            if(advicesRequest.isSuccessful) {
                if(!advicesRequest.body().isNullOrEmpty()) CurrentAppData.advices = advicesRequest.body()!!
            }
        }
        ImageDownloader.urlToBitmap(context, scope, UrlWheelBorder, onSuccess = {
            CurrentAppData.wheelBorder = it
            checkCompletion()
        }, onError = {
            mInterface?.onError()
        })
        ImageDownloader.urlToBitmap(context, scope, UrlWheelPin, onSuccess = {
            CurrentAppData.wheelPin = it
            checkCompletion()
        }, onError = {
            mInterface?.onError()
        })
    }

    private fun checkCompletion() {
        if(CurrentAppData.wheelBorder != null &&
                CurrentAppData.wheelPin != null) mInterface?.onAssetsLoaded()
    }


    interface AssetLoaderInterface {
        fun onAssetsLoaded()
        fun onError()
    }
}